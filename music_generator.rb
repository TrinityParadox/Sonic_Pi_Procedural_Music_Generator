use_random_seed Time.now.to_i
#You can change the seed by replacing Time.now.to_i with some number

I = chord(:C4, :major)
i = chord(:C4, :minor)
idim = chord(:C4,:dim)
Iaug = chord(:C4, :augmented)
Isus4 = chord(:C4,:sus4)
Isus2 = chord(:C4, :sus2)

ii = chord(:D4, :minor)
II = chord(:D4, :major)
IIsus4 = chord(:C4,:sus4)
iidim = chord(:D4, :dim)
IIb = chord(:Cf4, :major)
iib = chord(:Cf4, :minor)
iibdim = chord(:Cf4,:dim)
IIbaug = chord(:Cf4,:augmented)

iii = chord(:E4, :minor)
iiidim = chord(:E4,:dim)
III = chord(:E4, :major)
IIIb = chord(:Ef4, :major)
iiib = chord(:Ef4, :minor)

IV = chord_invert(chord(:F3, :major),2)
iv = chord_invert(chord(:F3, :minor),2)

V = chord_invert(chord(:G3, :major),2)
v = chord_invert(chord(:G3, :minor),2)
Vb = chord_invert(chord(:Gf3, :major),2)
vb = chord_invert(chord(:Gf3, :minor),2)

vi = chord_invert(chord(:A3, :minor),2)
VI = chord_invert(chord(:A3, :major),2)
vib = chord_invert(chord(:Af3, :minor),2)
VIb = chord_invert(chord(:Af3, :major),2)

viidim = chord_invert(chord(:B3, :diminished),2)
vii = chord_invert(chord(:B3, :minor),2)
VII = chord_invert(chord(:B3, :major),2)
VIIb = chord_invert(chord(:Bf2, :major),2)
viib = chord_invert(chord(:Bf2, :minor),2)

instrument = :saw
#The instrument

speed_of_notes = 0.25
#Speed at which the notes are strucked on the "keyboard"

music_by_brightness_and_darkness = 10
#1 to 9 goes from dark to bright
#10 means that it plays all 1 to 9 at random

time_signature_param = [2,6]
#Picking random time signature from 2/4,3/4,4/4,5/4,6/4 you can also edit this

repeating_param = [2,4]
#How many times should the melody be repeated from 2 to 4

from_to_bpm_param = [60,120]
#Picking from to beats per minute (speed of the tempo for short)

amplitude = 0.4
#Optional for volume

rel = 0.1
#Optional release

sus = 0.1
#Optional sustain

if music_by_brightness_and_darkness == 1 then
  progression_pick = [[idim,iidim,idim,iidim],
                      [idim,viidim,idim,viidim],
                      [idim,idim,idim,idim],
                      [idim,IIb,idim,IIb],
                      [idim,iib,idim,iib],
                      [viidim,II,viidim,II],
                      [idim,iibdim,idim,iibdim],
                      [Iaug,IIbaug,Iaug,IIbaug]]
elsif music_by_brightness_and_darkness == 2 then
  progression_pick = [[i,IIb,i,viib],
                      [I,IIb,I,viib],
                      [I,IIb,I,IIb],
                      [i,IIb,i,IIb],
                      [iii,ii,iii,ii],
                      [i,VI,i,VI]]
elsif music_by_brightness_and_darkness == 3 then
  progression_pick = [[vi,vi,IV,III],
                      [i,i,i,i],
                      [vi,IV,I,III],
                      [i,VIIb,VIb,V],
                      [i,VI,i,VI],
                      [i,II,IV,i],
                      [i,II,IV,I]]
elsif music_by_brightness_and_darkness == 4 then
  progression_pick = [[vi,ii,V,I],
                      [vi,IV,I,V],
                      [i,VIIb,v,VIb],
                      [ii,V,vi,IV],
                      [IV,V,vi,vi]]
elsif music_by_brightness_and_darkness == 5 then
  progression_pick = [[ii,V,I,I],
                      [ii,V,I,IV],
                      [IV,V,I,vi],
                      [IV,V,iii,vi],
                      [IV,V,III,vi],
                      [ii,V,ii,V],
                      [I,III,IV,iv],
                      [I,III,vi,IV],
                      [I,iii,IV,V],
                      [I,iii,iiidim,IV]]
elsif music_by_brightness_and_darkness == 6 then
  progression_pick = [[I,VIIb,I,VIIb],
                      [I,VIIb,IV,I],
                      [I,VIIb,IV,V],
                      [I,VIIb,iv,I],
                      [I,iv,I,iv],
                      [I,VIIb,iv,v],
                      [I,v,I,v],
                      [IV,VIb,VIIb,I]]
elsif music_by_brightness_and_darkness == 7 then
  progression_pick = [[I,V,vi,IV],
                      [I,V,ii,IV],
                      [I,IV,I,V],
                      [I,IV,I,IV],
                      [I,V,I,V],
                      [I,vi,IV,V],
                      [I,iibdim,ii,V],
                      [I,Iaug,ii,V],
                      [I,I,I,I]]
elsif music_by_brightness_and_darkness == 8 then
  progression_pick = [[I,II,IV,I],
                      [I,II,I,II],
                      [I,ii,IV,I],
                      [I,VI,I,VI]]
elsif music_by_brightness_and_darkness == 9 then
  progression_pick = [[Isus4,IIsus4,Isus4,IIsus4],
                      [Isus4,Isus2,Isus4,Isus2],
                      [I,Isus4,IIsus4,I],
                      [I,Isus4,I,Isus2]]
elsif music_by_brightness_and_darkness == 10 then
  progression_pick = [[Isus4,IIsus4,Isus4,IIsus4],
                      [Isus4,Isus2,Isus4,Isus2],
                      [I,Isus4,IIsus4,I],
                      [I,Isus4,I,Isus2],[I,II,IV,I],
                      [I,II,I,II],
                      [I,ii,IV,I],
                      [I,VI,I,VI],[I,V,vi,IV],
                      [I,V,ii,IV],
                      [I,IV,I,V],
                      [I,IV,I,IV],
                      [I,V,I,V],
                      [I,vi,IV,V],
                      [I,iibdim,ii,V],
                      [I,Iaug,ii,V],
                      [I,I,I,I],[I,VIIb,I,VIIb],
                      [I,VIIb,IV,I],
                      [I,VIIb,IV,V],
                      [I,VIIb,iv,I],
                      [I,iv,I,iv],
                      [I,VIIb,iv,v],
                      [I,v,I,v],
                      [IV,VIb,VIIb,I],[ii,V,I,I],
                      [ii,V,I,IV],
                      [IV,V,I,vi],
                      [IV,V,iii,vi],
                      [IV,V,III,vi],
                      [ii,V,ii,V],
                      [I,III,IV,iv],
                      [I,III,vi,IV],
                      [I,iii,IV,V],
                      [I,iii,iiidim,IV],[vi,ii,V,I],
                      [vi,IV,I,V],
                      [i,VIIb,v,VIb],
                      [ii,V,vi,IV],
                      [IV,V,vi,vi],[vi,vi,IV,III],
                      [i,i,i,i],
                      [vi,IV,I,III],
                      [i,VIIb,VIb,V],
                      [i,VI,i,VI],
                      [i,II,IV,i],
                      [i,II,IV,I],[i,IIb,i,viib],
                      [I,IIb,I,viib],
                      [I,IIb,I,IIb],
                      [i,IIb,i,IIb],
                      [iii,ii,iii,ii],
                      [i,VI,i,VI],[idim,iidim,idim,iidim],
                      [idim,viidim,idim,viidim],
                      [idim,idim,idim,idim],
                      [idim,IIb,idim,IIb],
                      [idim,iib,idim,iib],
                      [viidim,II,viidim,II],
                      [idim,iibdim,idim,iibdim],
                      [Iaug,IIbaug,Iaug,IIbaug]]
end

pit = rrand_i(0,6)
if pit == 0 then
  pit_melody = -12
elsif pit == 1 then
  pit_melody = -11
elsif pit == 2 then
  pit_melody = -10
elsif pit == 3 then
  pit_melody = -9
elsif pit == 4 then
  pit_melody = -8
elsif pit == 5 then
  pit_melody = -7
elsif pit == 6 then
  pit_melody = -6
elsif pit == 7 then
  pit_melody = -5
elsif pit == 8 then
  pit_melody = -4
elsif pit == 9 then
  pit_melody = -3
elsif pit == 10 then
  pit_melody = -2
elsif pit == 11 then
  pit_melody = -1
elsif pit == 12 then
  pit_melody = 0
end

time_signature = rrand_i(time_signature_param[0],time_signature_param[1])
beats_per_minute = rrand(from_to_bpm_param[0],from_to_bpm_param[1])
progression = progression_pick[rrand_i(0,progression_pick.size-1)]
repeating = rrand_i(repeating_param[0],repeating_param[1])

progression_1 = progression[0].pick(time_signature)
progression_2 = progression[1].pick(time_signature)
progression_3 = progression[2].pick(time_signature)
progression_4 = progression[3].pick(time_signature)

timing_1 = speed_of_notes
timing_2 = speed_of_notes
timing_3 = speed_of_notes
timing_4 = speed_of_notes

timer = 0

in_thread do
  with_bpm beats_per_minute do
    loop do
      use_synth instrument
      timer = timer + 1
      print("Repeats: ",timer)
      print("Time Signature: ",time_signature)
      print("Random Seed:", Time.now.to_i)
      play_chord progression[0], sustain: sus*3, release: rel*6, pitch: pit_melody, amp: amplitude*2
      play_pattern_timed progression_1.ring, timing_1, release: rel, sustain: sus, pitch: pit, amp: amplitude
      play_chord progression[1], sustain: sus*3, release: rel*6, pitch: pit_melody, amp: amplitude*2
      play_pattern_timed progression_2.ring,timing_2, release: rel, sustain: sus, pitch: pit, amp: amplitude
      play_chord progression[2], sustain: sus*3, release: rel*6, pitch: pit_melody, amp: amplitude*2
      play_pattern_timed progression_3.ring,timing_3, release: rel, sustain: sus, pitch: pit, amp: amplitude
      play_chord progression[3], sustain: sus*3, release: rel*6, pitch: pit_melody, amp: amplitude*2
      play_pattern_timed progression_4.ring,timing_4, release: rel, sustain: sus, pitch: pit, amp: amplitude
      if timer >= repeating then
        time_signature = rrand_i(time_signature_param[0],time_signature_param[1])
        beats_per_minute = rrand_i(from_to_bpm_param[0],from_to_bpm_param[1])
        progression = progression_pick[rrand_i(0,progression_pick.size-1)]
        progression_1 = progression[0].pick(time_signature)
        progression_2 = progression[1].pick(time_signature)
        progression_3 = progression[2].pick(time_signature)
        progression_4 = progression[3].pick(time_signature)
        timing_1 = speed_of_notes
        timing_2 = speed_of_notes
        timing_3 = speed_of_notes
        timing_4 = speed_of_notes
        repeating = rrand_i(repeating_param[0],repeating_param[1])
        timer = 0
        
      end
    end
  end
end